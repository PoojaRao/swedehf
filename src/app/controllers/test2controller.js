(function () {
    angular
        .module('app')
        .controller('test2Controller', ['testService',
            test2Controller
        ]);
    function test2Controller(testService) {
      var vm = this;
        vm.data=testService.data;
    }
  })();
