(function () {
    angular
        .module('app')
        .controller('testController', ['testService',  '$http',
            testController
        ]);
    function testController(testService, $http) {
      var vm = this;
        vm.getPred = function(){
          vm.data.press += 1
          vm.data.cluster = ( "http://localhost:8001/cluster?age="+ String(vm.data.age)
          + "&weight=" +String(vm.data.weight)
          + "&hb=" +String(vm.data.hb)
          + "&creat=" +String(vm.data.creat)
          + "&SBP=" +String(vm.data.SBP)
          + "&MAP=" +String(vm.data.DBP)
          + "&HR=" +String(vm.data.HR)
          + "&dispInc=" +String(vm.data.dispInc));

          vm.data.predict = ( "http://localhost:8001/predict_surv?age="+ String(vm.data.age)
          + "&weight=" +String(vm.data.weight)
          + "&hb=" +String(vm.data.hb)
          + "&creat=" +String(vm.data.creat)
          + "&SBP=" +String(vm.data.SBP)
          + "&MAP=" +String(vm.data.DBP)
          + "&HR=" +String(vm.data.HR)
          + "&dispInc=" +String(vm.data.dispInc));
          console.log(vm.data.cluster)
          $http({
        method : "GET",
        url : vm.data.cluster
      }).then(function mySucces(response) {
        vm.data.myCluster = response.data;

      }, function myError(response) {
        vm.data.myCluster = response.statusText;
      });

        $http({
      method : "GET",
      url : vm.data.predict
    }).then(function mySucces(response) {
      vm.data.mySurv = response.data;
    }, function myError(response) {
      vm.data.mySurv = response.statusText;
    });
      };
        vm.data=testService.data


    }
  })();
