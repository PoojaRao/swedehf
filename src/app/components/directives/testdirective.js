'use strict';

angular.module('app')
  .directive('survivalChart', function($window, testService) {
    return{
      restrict: "EA",
      template: "<svg width='600' height='400' viewBox='0 0 600 400'></svg>",
      link: function(scope, elem, attrs){
        var selectedCluster = "cluster3";

        scope.$watch(function(){
          return testService.data.myCluster;
        }, function(newVal, oldVal){
          if(newVal)
          var selectedCluster = testService.data.myCluster;
          console.log(selectedCluster);
        d3.selectAll("path.line").remove();

        var svg = d3.select("svg"),
            margin = {top: 20, right: 80, bottom: 40, left: 50},
            width = svg.attr("width") - margin.left - margin.right,
            height = svg.attr("height") - margin.top - margin.bottom,
            g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.scaleLinear().domain([0, width]).range([0, width]),
            y = d3.scaleLinear().range([height, 0]),
            z = d3.scaleOrdinal(d3.schemeCategory10);

        var line = d3.line()
            .curve(d3.curveBasis)
            .x(function(d) { return x(d.time); })
            .y(function(d) { return y(d.survival); });

            d3.tsv("app/data/survival.csv", type, function(error, data) {
              if (error) throw error;

              var clusters = data.columns.slice(1).map(function(id) {
                return {
                  id: id,
                  values: data.map(function(d) {
                    return {time: d.time, survival: d[id]};
                  })
                };
              });


            x.domain([0,12]);
            y.domain([0,1]);
            z.domain(clusters.map(function(c) { return c.id; }));

            g.append("g")
                .attr("class", "axis axis--x")
                .attr("transform", "translate(0," + height + ")")
                .attr("stroke" ,"6C6C6C")
                .attr("font-family", "Roboto")
                .call(d3.axisBottom(x).tickSize(0))
              .append("text")
              .attr("y", 20)
              .attr("x", width / 2 )
              .attr("dy", "0.61em")
              .attr("fill", "#000")
              .attr("font-size", "1.7em")
              .attr("font-family", "Roboto")
              .text("Years");

              g.append("g")
                  .attr("class", "axis axis--y")
                  .call(d3.axisLeft(y).tickSize(0))
                  .attr("fill", "#D3D3D3")
                  .attr("font-family", "Roboto")
                .append("text")
                  .attr("transform", "rotate(-90)")
                  .attr("y", -36)
                  .attr("x", -height/2)
                  .attr("dy", "0.61em")
                  .attr("fill", "#000")
                  .attr("font-size", "1.7em")
                  .attr("font-family", "Roboto")
                  .text("Survival");

              var cluster = g.selectAll(".cluster")
                .data(clusters)
                .enter().append("g")
                  .attr("class", "cluster");

              // cluster.append("path")
              //     .attr("class", "line")
              //     .attr("d", function(d) { return line(d.values); })
              //     .style("stroke", function(d) {
              //                       return "lightgrey"
              //             ;})


              cluster.append("path")
                  .attr("class", "line")
                  .attr("d", function(d) { return line(d.values); })
                  .style("stroke", function(d) {
                              if (d.id == selectedCluster) {return "#E75753"}
                              else 	{ return "lightgrey" }
                          ;})



              cluster.append("text")
                  .datum(function(d) { return {id: d.id, value: d.values[d.values.length - 1]}; })
                  .attr("transform", function(d) { return "translate(" + x(d.value.time) + "," + y(d.value.survival) + ")"; })
                  .attr("x", 3)
                  .attr("dy", "0.35em")
                  .style("font", "1em Segoe UI")
                  .text(function(d) { return d.id; });
            });

            function type(d, _, columns) {
              d.time = d.time;
              for (var i = 1, n = columns.length, c; i < n; ++i) d[c = columns[i]] = +d[c];
              return d;
            }
            });
      }
    };
  });
